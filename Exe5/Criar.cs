﻿using LinqBd;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exe5
{
    public partial class Criar : Form
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();
        public Criar()
        {
            InitializeComponent();
        }

        private void Criar_Load(object sender, EventArgs e)
        {
            ComboBoxCriarDepartamentos.DataSource = dc.Departamentos;
            ComboBoxCriarDepartamentos.DisplayMember = "Sigla";
        }

        private void ButtonCriarFunc_Click(object sender, EventArgs e)
        {
            GroupBoxCriar.Enabled = true;
            GroupBoxCriarDepartamentos.Enabled = false;
            ButtonCriarDep.Enabled = false;

        }

        private void ButtonCriarDep_Click(object sender, EventArgs e)
        {
            GroupBoxCriarDepartamentos.Enabled = true;
            GroupBoxCriar.Enabled = false;
            ButtonCriarFunc.Enabled = false;

        }

        private int cont = 100;
        private void ButtonCriar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TextBoxCriarNome.Text))
            {
                MessageBox.Show("Insira um nome!");
                return;
            }

            if (ComboBoxCriarDepartamentos.SelectedIndex == -1)
            {
                MessageBox.Show("Escolha um departamento!");
                return;
            }


            Funcionario func = new Funcionario
            {
                ID = dc.Funcionarios.Count() + cont,
                Nome = TextBoxCriarNome.Text,
                Departamento = ComboBoxCriarDepartamentos.Text

            };


            dc.Funcionarios.InsertOnSubmit(func);

            try
            {
                dc.SubmitChanges();
            }
            catch (Exception p)
            {
                MessageBox.Show(p.Message);

            }

            GroupBoxCriar.Enabled = false;
            ButtonCriarDep.Enabled = true;
            TextBoxCriarNome.Clear();
            cont++;
        }

        private void ButtonCalcelarCriarFunc_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Não foram efectuadas alterações.");
            ButtonCriarDep.Enabled = true;
            TextBoxCriarNome.Clear();
            GroupBoxCriar.Enabled = false;

        }


        private void buttonCancelarCriarDepartamento_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Não foram efectuadas alterações.");
            ButtonCriarFunc.Enabled = true;
            TextBoxCriarDepartamento.Clear();
            TextBoxCriarSigla.Clear();
            GroupBoxCriarDepartamentos.Enabled = false;
        }

        private void ButtonCriarDepartamento_Click(object sender, EventArgs e)
        {
            var lista = from Departamento
                        in dc.Departamentos
                        where TextBoxCriarDepartamento.Text == Departamento.Departamento1
                        select Departamento;

            var lista1 = from Departamento
                         in dc.Departamentos
                         where TextBoxCriarSigla.Text == Departamento.Sigla
                         select Departamento;

           
            if (string.IsNullOrEmpty(TextBoxCriarDepartamento.Text) || lista.Count() > 0)
            {
                MessageBox.Show("Insira um Departamento que não exista!");
                return;
            }
            if (string.IsNullOrEmpty(TextBoxCriarSigla.Text) || TextBoxCriarSigla.Text.Length > 3 || lista1.Count() > 0)
            {
                MessageBox.Show("Insira uma Sigla, com 3 caracteres 'Máximo', que não exista!");
                return;
            }


            Departamento dep = new Departamento
            {
                Sigla = TextBoxCriarSigla.Text,

                Departamento1 = TextBoxCriarDepartamento.Text

            };


            dc.Departamentos.InsertOnSubmit(dep);

            try
            {
                dc.SubmitChanges();
            }
            catch (Exception p)
            {
                MessageBox.Show(p.Message);

            }

            GroupBoxCriarDepartamentos.Enabled = false;
            ButtonCriarFunc.Enabled = true;
            TextBoxCriarDepartamento.Clear();
            TextBoxCriarSigla.Clear();
        }
    }
}
