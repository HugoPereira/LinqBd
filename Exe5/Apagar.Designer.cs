﻿namespace Exe5
{
    partial class Apagar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GroupBoxFunc = new System.Windows.Forms.GroupBox();
            this.ComboBoxIdFunc = new System.Windows.Forms.ComboBox();
            this.ComboBoxDepFunc = new System.Windows.Forms.ComboBox();
            this.ButtonApagaFunc = new System.Windows.Forms.Button();
            this.ComboBoxFunc = new System.Windows.Forms.ComboBox();
            this.ButtonFunc = new System.Windows.Forms.Button();
            this.ButtonDep = new System.Windows.Forms.Button();
            this.GroupBoxDep = new System.Windows.Forms.GroupBox();
            this.ComboBoxSigla = new System.Windows.Forms.ComboBox();
            this.ButtonApagarDep = new System.Windows.Forms.Button();
            this.ComboBoxDep = new System.Windows.Forms.ComboBox();
            this.ButtonCloseFunc = new System.Windows.Forms.Button();
            this.ButtonCloseDep = new System.Windows.Forms.Button();
            this.GroupBoxFunc.SuspendLayout();
            this.GroupBoxDep.SuspendLayout();
            this.SuspendLayout();
            // 
            // GroupBoxFunc
            // 
            this.GroupBoxFunc.Controls.Add(this.ButtonCloseFunc);
            this.GroupBoxFunc.Controls.Add(this.ComboBoxIdFunc);
            this.GroupBoxFunc.Controls.Add(this.ComboBoxDepFunc);
            this.GroupBoxFunc.Controls.Add(this.ButtonApagaFunc);
            this.GroupBoxFunc.Controls.Add(this.ComboBoxFunc);
            this.GroupBoxFunc.Enabled = false;
            this.GroupBoxFunc.Location = new System.Drawing.Point(195, 12);
            this.GroupBoxFunc.Name = "GroupBoxFunc";
            this.GroupBoxFunc.Size = new System.Drawing.Size(434, 148);
            this.GroupBoxFunc.TabIndex = 0;
            this.GroupBoxFunc.TabStop = false;
            // 
            // ComboBoxIdFunc
            // 
            this.ComboBoxIdFunc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxIdFunc.FormattingEnabled = true;
            this.ComboBoxIdFunc.Location = new System.Drawing.Point(10, 19);
            this.ComboBoxIdFunc.Name = "ComboBoxIdFunc";
            this.ComboBoxIdFunc.Size = new System.Drawing.Size(53, 21);
            this.ComboBoxIdFunc.TabIndex = 3;
            // 
            // ComboBoxDepFunc
            // 
            this.ComboBoxDepFunc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxDepFunc.FormattingEnabled = true;
            this.ComboBoxDepFunc.Location = new System.Drawing.Point(263, 19);
            this.ComboBoxDepFunc.Name = "ComboBoxDepFunc";
            this.ComboBoxDepFunc.Size = new System.Drawing.Size(154, 21);
            this.ComboBoxDepFunc.TabIndex = 2;
            // 
            // ButtonApagaFunc
            // 
            this.ButtonApagaFunc.Image = global::Exe5.Properties.Resources.icon_save;
            this.ButtonApagaFunc.Location = new System.Drawing.Point(342, 57);
            this.ButtonApagaFunc.Name = "ButtonApagaFunc";
            this.ButtonApagaFunc.Size = new System.Drawing.Size(63, 52);
            this.ButtonApagaFunc.TabIndex = 1;
            this.ButtonApagaFunc.UseVisualStyleBackColor = true;
            this.ButtonApagaFunc.Click += new System.EventHandler(this.ButtonApagaFunc_Click);
            // 
            // ComboBoxFunc
            // 
            this.ComboBoxFunc.FormattingEnabled = true;
            this.ComboBoxFunc.Location = new System.Drawing.Point(77, 19);
            this.ComboBoxFunc.Name = "ComboBoxFunc";
            this.ComboBoxFunc.Size = new System.Drawing.Size(173, 21);
            this.ComboBoxFunc.TabIndex = 0;
            this.ComboBoxFunc.SelectedIndexChanged += new System.EventHandler(this.ComboBoxFunc_SelectedIndexChanged);
            // 
            // ButtonFunc
            // 
            this.ButtonFunc.Location = new System.Drawing.Point(54, 42);
            this.ButtonFunc.Name = "ButtonFunc";
            this.ButtonFunc.Size = new System.Drawing.Size(97, 52);
            this.ButtonFunc.TabIndex = 3;
            this.ButtonFunc.Text = "Funcionário";
            this.ButtonFunc.UseVisualStyleBackColor = true;
            this.ButtonFunc.Click += new System.EventHandler(this.ButtonFunc_Click);
            // 
            // ButtonDep
            // 
            this.ButtonDep.Location = new System.Drawing.Point(54, 202);
            this.ButtonDep.Name = "ButtonDep";
            this.ButtonDep.Size = new System.Drawing.Size(97, 52);
            this.ButtonDep.TabIndex = 4;
            this.ButtonDep.Text = "Departamento";
            this.ButtonDep.UseVisualStyleBackColor = true;
            this.ButtonDep.Click += new System.EventHandler(this.ButtonDep_Click);
            // 
            // GroupBoxDep
            // 
            this.GroupBoxDep.Controls.Add(this.ButtonCloseDep);
            this.GroupBoxDep.Controls.Add(this.ComboBoxSigla);
            this.GroupBoxDep.Controls.Add(this.ButtonApagarDep);
            this.GroupBoxDep.Controls.Add(this.ComboBoxDep);
            this.GroupBoxDep.Enabled = false;
            this.GroupBoxDep.Location = new System.Drawing.Point(195, 166);
            this.GroupBoxDep.Name = "GroupBoxDep";
            this.GroupBoxDep.Size = new System.Drawing.Size(434, 163);
            this.GroupBoxDep.TabIndex = 2;
            this.GroupBoxDep.TabStop = false;
            // 
            // ComboBoxSigla
            // 
            this.ComboBoxSigla.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxSigla.FormattingEnabled = true;
            this.ComboBoxSigla.Location = new System.Drawing.Point(24, 19);
            this.ComboBoxSigla.Name = "ComboBoxSigla";
            this.ComboBoxSigla.Size = new System.Drawing.Size(121, 21);
            this.ComboBoxSigla.TabIndex = 2;
            // 
            // ButtonApagarDep
            // 
            this.ButtonApagarDep.Image = global::Exe5.Properties.Resources.icon_save;
            this.ButtonApagarDep.Location = new System.Drawing.Point(342, 63);
            this.ButtonApagarDep.Name = "ButtonApagarDep";
            this.ButtonApagarDep.Size = new System.Drawing.Size(63, 52);
            this.ButtonApagarDep.TabIndex = 1;
            this.ButtonApagarDep.UseVisualStyleBackColor = true;
            this.ButtonApagarDep.Click += new System.EventHandler(this.ButtonApagarDep_Click);
            // 
            // ComboBoxDep
            // 
            this.ComboBoxDep.FormattingEnabled = true;
            this.ComboBoxDep.Location = new System.Drawing.Point(169, 19);
            this.ComboBoxDep.Name = "ComboBoxDep";
            this.ComboBoxDep.Size = new System.Drawing.Size(248, 21);
            this.ComboBoxDep.TabIndex = 0;
            // 
            // ButtonCloseFunc
            // 
            this.ButtonCloseFunc.Image = global::Exe5.Properties.Resources.ic_cancel;
            this.ButtonCloseFunc.Location = new System.Drawing.Point(401, 124);
            this.ButtonCloseFunc.Name = "ButtonCloseFunc";
            this.ButtonCloseFunc.Size = new System.Drawing.Size(33, 24);
            this.ButtonCloseFunc.TabIndex = 4;
            this.ButtonCloseFunc.UseVisualStyleBackColor = true;
            this.ButtonCloseFunc.Click += new System.EventHandler(this.ButtonCloseFunc_Click);
            // 
            // ButtonCloseDep
            // 
            this.ButtonCloseDep.Image = global::Exe5.Properties.Resources.ic_cancel;
            this.ButtonCloseDep.Location = new System.Drawing.Point(401, 139);
            this.ButtonCloseDep.Name = "ButtonCloseDep";
            this.ButtonCloseDep.Size = new System.Drawing.Size(33, 24);
            this.ButtonCloseDep.TabIndex = 5;
            this.ButtonCloseDep.UseVisualStyleBackColor = true;
            this.ButtonCloseDep.Click += new System.EventHandler(this.ButtonCloseDep_Click);
            // 
            // Apagar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(668, 341);
            this.Controls.Add(this.GroupBoxDep);
            this.Controls.Add(this.ButtonDep);
            this.Controls.Add(this.ButtonFunc);
            this.Controls.Add(this.GroupBoxFunc);
            this.Name = "Apagar";
            this.Text = "Apagar";
            this.Load += new System.EventHandler(this.Apagar_Load);
            this.GroupBoxFunc.ResumeLayout(false);
            this.GroupBoxDep.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GroupBoxFunc;
        private System.Windows.Forms.ComboBox ComboBoxFunc;
        private System.Windows.Forms.Button ButtonApagaFunc;
        private System.Windows.Forms.Button ButtonFunc;
        private System.Windows.Forms.Button ButtonDep;
        private System.Windows.Forms.GroupBox GroupBoxDep;
        private System.Windows.Forms.Button ButtonApagarDep;
        private System.Windows.Forms.ComboBox ComboBoxDep;
        private System.Windows.Forms.ComboBox ComboBoxDepFunc;
        private System.Windows.Forms.ComboBox ComboBoxIdFunc;
        private System.Windows.Forms.ComboBox ComboBoxSigla;
        private System.Windows.Forms.Button ButtonCloseFunc;
        private System.Windows.Forms.Button ButtonCloseDep;
    }
}