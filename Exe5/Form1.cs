﻿using LinqBd;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exe5
{
    public partial class CRUDBd : Form
    {
        private Editar formEditar;
        private Criar formCriar;
        private Ver formVer;
        private Apagar formApagar;

       

        public CRUDBd()
        {
            InitializeComponent();
           
        }

        private void CRUDBd_Load(object sender, EventArgs e)
        {
           
        }


        private void ButtonCriar_Click_1(object sender, EventArgs e)
        {
            formCriar = new Criar();
            formCriar.Show();
        }

        private void ButtonVer_Click(object sender, EventArgs e)
        {
            formVer = new Ver();
            formVer.Show();
        }

        private void ButtonUpdate_Click(object sender, EventArgs e)
        {
            formEditar = new Editar();
            formEditar.Show();
        }

        private void ButtonDelete_Click(object sender, EventArgs e)
        {
            formApagar = new Apagar();
            formApagar.Show();
        }
    }
}
