﻿namespace Exe5
{
    partial class Editar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.GroupBoxFunc = new System.Windows.Forms.GroupBox();
            this.ComboBoxDepartamento = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TextBoxNome = new System.Windows.Forms.TextBox();
            this.ButtonGravarFunc = new System.Windows.Forms.Button();
            this.CuttonClose = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.GroupBoxFunc.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 58);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(343, 313);
            this.dataGridView1.TabIndex = 2;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // GroupBoxFunc
            // 
            this.GroupBoxFunc.Controls.Add(this.CuttonClose);
            this.GroupBoxFunc.Controls.Add(this.ComboBoxDepartamento);
            this.GroupBoxFunc.Controls.Add(this.label2);
            this.GroupBoxFunc.Controls.Add(this.label1);
            this.GroupBoxFunc.Controls.Add(this.TextBoxNome);
            this.GroupBoxFunc.Controls.Add(this.ButtonGravarFunc);
            this.GroupBoxFunc.Location = new System.Drawing.Point(389, 58);
            this.GroupBoxFunc.Name = "GroupBoxFunc";
            this.GroupBoxFunc.Size = new System.Drawing.Size(454, 142);
            this.GroupBoxFunc.TabIndex = 3;
            this.GroupBoxFunc.TabStop = false;
            this.GroupBoxFunc.Visible = false;
            // 
            // ComboBoxDepartamento
            // 
            this.ComboBoxDepartamento.FormattingEnabled = true;
            this.ComboBoxDepartamento.Location = new System.Drawing.Point(97, 58);
            this.ComboBoxDepartamento.Name = "ComboBoxDepartamento";
            this.ComboBoxDepartamento.Size = new System.Drawing.Size(272, 21);
            this.ComboBoxDepartamento.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Departamento:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Nome:";
            // 
            // TextBoxNome
            // 
            this.TextBoxNome.Location = new System.Drawing.Point(97, 19);
            this.TextBoxNome.Name = "TextBoxNome";
            this.TextBoxNome.Size = new System.Drawing.Size(272, 20);
            this.TextBoxNome.TabIndex = 1;
            // 
            // ButtonGravarFunc
            // 
            this.ButtonGravarFunc.Image = global::Exe5.Properties.Resources.icon_save;
            this.ButtonGravarFunc.Location = new System.Drawing.Point(392, 22);
            this.ButtonGravarFunc.Name = "ButtonGravarFunc";
            this.ButtonGravarFunc.Size = new System.Drawing.Size(49, 51);
            this.ButtonGravarFunc.TabIndex = 0;
            this.ButtonGravarFunc.UseVisualStyleBackColor = true;
            this.ButtonGravarFunc.Click += new System.EventHandler(this.ButtonGravar_Click);
            // 
            // CuttonClose
            // 
            this.CuttonClose.Image = global::Exe5.Properties.Resources.ic_cancel;
            this.CuttonClose.Location = new System.Drawing.Point(425, 119);
            this.CuttonClose.Name = "CuttonClose";
            this.CuttonClose.Size = new System.Drawing.Size(29, 23);
            this.CuttonClose.TabIndex = 6;
            this.CuttonClose.UseVisualStyleBackColor = true;
            this.CuttonClose.Click += new System.EventHandler(this.CuttonClose_Click);
            // 
            // Editar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(855, 410);
            this.Controls.Add(this.GroupBoxFunc);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Editar";
            this.Text = "Editar";
            this.Load += new System.EventHandler(this.Editar_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.GroupBoxFunc.ResumeLayout(false);
            this.GroupBoxFunc.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox GroupBoxFunc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TextBoxNome;
        private System.Windows.Forms.Button ButtonGravarFunc;
        private System.Windows.Forms.ComboBox ComboBoxDepartamento;
        private System.Windows.Forms.Button CuttonClose;
    }
}