﻿using LinqBd;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exe5
{
    public partial class Ver : Form
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();
        public Ver()
        {
            InitializeComponent();
        }

        private void criar_Load(object sender, EventArgs e)
        {
            #region Listview1
            listView1.Columns.Add("Id");
            listView1.Columns.Add("Nome");
            listView1.Columns.Add("Departamento");



            var lista = from Funcionario
                        in dc.Funcionarios
                        select Funcionario;


            foreach (Funcionario func in lista)
            {

                ListViewItem item;//criamos um item do tipo listviewitem

                item = listView1.Items.Add(func.ID.ToString());//insermos os campos nas colunas, primeiro campo
                item.SubItems.Add(func.Nome);//campos restantes
                item.SubItems.Add(func.Departamento);

            }

            for (int idx = 0; idx <= 2; idx++)//ajustar as coluna ao conteudo
            {
                listView1.Columns[idx].AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
            }
            #endregion

            #region datagridview

            dataGridView1.Columns.Add("colSigla", "Sigla"); //primeiro o nome da coluna e dps o titulo do cabeçalho da coluna

            dataGridView1.Columns.Add("colDepartamento", "Departamento");

            var outraLista3 = from Departamento in dc.Departamentos select Departamento;

            int idxlinha = 0;



            foreach (Departamento func in outraLista3)//por cada funcionario na lista
            {
                DataGridViewRow linha = new DataGridViewRow();//cria uma coluna

                dataGridView1.Rows.Add(linha);//insere as colunas na grid

                dataGridView1.Rows[idxlinha].Cells[0].Value = func.Sigla;// o valor da coluna 0, sera o id

                dataGridView1.Rows[idxlinha].Cells[1].Value = func.Departamento1; //o valor da coluna 2, sera o departamento
                idxlinha++;
            }

            //ajusta o tamanho das colunas por codigo
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            #endregion
        }
    }
}
