﻿using LinqBd;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exe5
{
    public partial class Apagar : Form
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();

        public Apagar()
        {
            InitializeComponent();
        }

        private void Apagar_Load(object sender, EventArgs e)
        {
            ComboBoxIdFunc.DataSource = dc.Funcionarios;
            ComboBoxIdFunc.DisplayMember = "ID";

            ComboBoxFunc.DataSource = dc.Funcionarios;
            ComboBoxFunc.DisplayMember = "Nome";

            ComboBoxDepFunc.DataSource = dc.Funcionarios;
            ComboBoxDepFunc.DisplayMember = "Departamento";


            ComboBoxDep.DataSource = dc.Departamentos;
            ComboBoxDep.DisplayMember = "Departamento1";

            ComboBoxSigla.DataSource = dc.Departamentos;
            ComboBoxSigla.DisplayMember = "Sigla";
        }

        private void ButtonApagarDep_Click(object sender, EventArgs e)
        {
            Departamento d = new Departamento();

            Funcionario f = new Funcionario();

            var func = from Funcionario in dc.Funcionarios where Funcionario.Departamento == ComboBoxSigla.Text select Funcionario;

            if (func.Count() > 0)
            {
                MessageBox.Show("Tem funcionarios neste departamento. altere o departamento desses funcionarios ou elimine os funcionarios");
                return;
            }
            
            var Pesquisa = from Departamento in dc.Departamentos
                                where Departamento.Sigla == ComboBoxSigla.Text
                                select Departamento;

            d = Pesquisa.Single();

            dc.Departamentos.DeleteOnSubmit(d);//deixa o eliminar pendente

            try
            {
                dc.SubmitChanges();//actualiza a lista, ja como elemento apagado
            }
            catch (Exception p)
            {
                Console.WriteLine(p.Message);
            }

            Close();
        }

        private void ButtonApagaFunc_Click(object sender, EventArgs e)
        {
           
            Funcionario f = new Funcionario();


            var Pesquisa = from Funcionario in dc.Funcionarios
                           where Funcionario.ID == Convert.ToInt32(ComboBoxIdFunc.Text)
                           select Funcionario;
            
            f = Pesquisa.Single();

            dc.Funcionarios.DeleteOnSubmit(f);//deixa o eliminar pendente

            try
            {
                dc.SubmitChanges();//actualiza a lista, ja como elemento apagado
            }
            catch (Exception p)
            {
                Console.WriteLine(p.Message);
            }

            Close();
        }

        private void ComboBoxFunc_SelectedIndexChanged(object sender, EventArgs e)
        {
           
           
        }

        private void ButtonFunc_Click(object sender, EventArgs e)
        {
            GroupBoxFunc.Enabled = true;
            ButtonDep.Enabled = false;
        }

        private void ButtonDep_Click(object sender, EventArgs e)
        {
            GroupBoxDep.Enabled = true;
            ButtonFunc.Enabled = false;
        }

        private void ButtonCloseFunc_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Não foram efectuadas alterações.");
            ButtonFunc.Enabled = true;
            ButtonDep.Enabled = true;
            GroupBoxFunc.Enabled = false;
        }

        private void ButtonCloseDep_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Não foram efectuadas alterações.");
            ButtonDep.Enabled = true;
            ButtonFunc.Enabled = true;
            GroupBoxDep.Enabled = false;
        }
    }

}
