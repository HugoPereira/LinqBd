﻿namespace Exe5
{
    partial class Criar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GroupBoxCriarDepartamentos = new System.Windows.Forms.GroupBox();
            this.buttonCancelarCriarDepartamento = new System.Windows.Forms.Button();
            this.ButtonCriarDepartamento = new System.Windows.Forms.Button();
            this.TextBoxCriarSigla = new System.Windows.Forms.TextBox();
            this.TextBoxCriarDepartamento = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.GroupBoxCriar = new System.Windows.Forms.GroupBox();
            this.ButtonCalcelarCriarFunc = new System.Windows.Forms.Button();
            this.ButtonCriar = new System.Windows.Forms.Button();
            this.ComboBoxCriarDepartamentos = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TextBoxCriarNome = new System.Windows.Forms.TextBox();
            this.ButtonCriarFunc = new System.Windows.Forms.Button();
            this.ButtonCriarDep = new System.Windows.Forms.Button();
            this.GroupBoxCriarDepartamentos.SuspendLayout();
            this.GroupBoxCriar.SuspendLayout();
            this.SuspendLayout();
            // 
            // GroupBoxCriarDepartamentos
            // 
            this.GroupBoxCriarDepartamentos.Controls.Add(this.buttonCancelarCriarDepartamento);
            this.GroupBoxCriarDepartamentos.Controls.Add(this.ButtonCriarDepartamento);
            this.GroupBoxCriarDepartamentos.Controls.Add(this.TextBoxCriarSigla);
            this.GroupBoxCriarDepartamentos.Controls.Add(this.TextBoxCriarDepartamento);
            this.GroupBoxCriarDepartamentos.Controls.Add(this.label4);
            this.GroupBoxCriarDepartamentos.Controls.Add(this.label3);
            this.GroupBoxCriarDepartamentos.Enabled = false;
            this.GroupBoxCriarDepartamentos.Location = new System.Drawing.Point(143, 211);
            this.GroupBoxCriarDepartamentos.Name = "GroupBoxCriarDepartamentos";
            this.GroupBoxCriarDepartamentos.Size = new System.Drawing.Size(427, 167);
            this.GroupBoxCriarDepartamentos.TabIndex = 5;
            this.GroupBoxCriarDepartamentos.TabStop = false;
            // 
            // buttonCancelarCriarDepartamento
            // 
            this.buttonCancelarCriarDepartamento.Image = global::Exe5.Properties.Resources.ic_cancel;
            this.buttonCancelarCriarDepartamento.Location = new System.Drawing.Point(396, 144);
            this.buttonCancelarCriarDepartamento.Name = "buttonCancelarCriarDepartamento";
            this.buttonCancelarCriarDepartamento.Size = new System.Drawing.Size(31, 23);
            this.buttonCancelarCriarDepartamento.TabIndex = 6;
            this.buttonCancelarCriarDepartamento.UseVisualStyleBackColor = true;
            this.buttonCancelarCriarDepartamento.Click += new System.EventHandler(this.buttonCancelarCriarDepartamento_Click);
            // 
            // ButtonCriarDepartamento
            // 
            this.ButtonCriarDepartamento.Image = global::Exe5.Properties.Resources.icon_create;
            this.ButtonCriarDepartamento.Location = new System.Drawing.Point(327, 26);
            this.ButtonCriarDepartamento.Name = "ButtonCriarDepartamento";
            this.ButtonCriarDepartamento.Size = new System.Drawing.Size(75, 60);
            this.ButtonCriarDepartamento.TabIndex = 9;
            this.ButtonCriarDepartamento.UseVisualStyleBackColor = true;
            this.ButtonCriarDepartamento.Click += new System.EventHandler(this.ButtonCriarDepartamento_Click);
            // 
            // TextBoxCriarSigla
            // 
            this.TextBoxCriarSigla.Location = new System.Drawing.Point(102, 66);
            this.TextBoxCriarSigla.Name = "TextBoxCriarSigla";
            this.TextBoxCriarSigla.Size = new System.Drawing.Size(185, 20);
            this.TextBoxCriarSigla.TabIndex = 8;
            // 
            // TextBoxCriarDepartamento
            // 
            this.TextBoxCriarDepartamento.Location = new System.Drawing.Point(102, 26);
            this.TextBoxCriarDepartamento.Name = "TextBoxCriarDepartamento";
            this.TextBoxCriarDepartamento.Size = new System.Drawing.Size(185, 20);
            this.TextBoxCriarDepartamento.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Sigla:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Departamento:";
            // 
            // GroupBoxCriar
            // 
            this.GroupBoxCriar.Controls.Add(this.ButtonCalcelarCriarFunc);
            this.GroupBoxCriar.Controls.Add(this.ButtonCriar);
            this.GroupBoxCriar.Controls.Add(this.ComboBoxCriarDepartamentos);
            this.GroupBoxCriar.Controls.Add(this.label2);
            this.GroupBoxCriar.Controls.Add(this.label1);
            this.GroupBoxCriar.Controls.Add(this.TextBoxCriarNome);
            this.GroupBoxCriar.Enabled = false;
            this.GroupBoxCriar.Location = new System.Drawing.Point(143, 37);
            this.GroupBoxCriar.Name = "GroupBoxCriar";
            this.GroupBoxCriar.Size = new System.Drawing.Size(427, 156);
            this.GroupBoxCriar.TabIndex = 4;
            this.GroupBoxCriar.TabStop = false;
            // 
            // ButtonCalcelarCriarFunc
            // 
            this.ButtonCalcelarCriarFunc.Image = global::Exe5.Properties.Resources.ic_cancel;
            this.ButtonCalcelarCriarFunc.Location = new System.Drawing.Point(396, 133);
            this.ButtonCalcelarCriarFunc.Name = "ButtonCalcelarCriarFunc";
            this.ButtonCalcelarCriarFunc.Size = new System.Drawing.Size(31, 23);
            this.ButtonCalcelarCriarFunc.TabIndex = 5;
            this.ButtonCalcelarCriarFunc.UseVisualStyleBackColor = true;
            this.ButtonCalcelarCriarFunc.Click += new System.EventHandler(this.ButtonCalcelarCriarFunc_Click);
            // 
            // ButtonCriar
            // 
            this.ButtonCriar.Image = global::Exe5.Properties.Resources.icon_create;
            this.ButtonCriar.Location = new System.Drawing.Point(327, 24);
            this.ButtonCriar.Name = "ButtonCriar";
            this.ButtonCriar.Size = new System.Drawing.Size(75, 63);
            this.ButtonCriar.TabIndex = 4;
            this.ButtonCriar.UseVisualStyleBackColor = true;
            this.ButtonCriar.Click += new System.EventHandler(this.ButtonCriar_Click);
            // 
            // ComboBoxCriarDepartamentos
            // 
            this.ComboBoxCriarDepartamentos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxCriarDepartamentos.FormattingEnabled = true;
            this.ComboBoxCriarDepartamentos.Location = new System.Drawing.Point(101, 66);
            this.ComboBoxCriarDepartamentos.Name = "ComboBoxCriarDepartamentos";
            this.ComboBoxCriarDepartamentos.Size = new System.Drawing.Size(186, 21);
            this.ComboBoxCriarDepartamentos.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Departamento:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nome:";
            // 
            // TextBoxCriarNome
            // 
            this.TextBoxCriarNome.Location = new System.Drawing.Point(62, 24);
            this.TextBoxCriarNome.Name = "TextBoxCriarNome";
            this.TextBoxCriarNome.Size = new System.Drawing.Size(225, 20);
            this.TextBoxCriarNome.TabIndex = 0;
            // 
            // ButtonCriarFunc
            // 
            this.ButtonCriarFunc.Location = new System.Drawing.Point(29, 80);
            this.ButtonCriarFunc.Name = "ButtonCriarFunc";
            this.ButtonCriarFunc.Size = new System.Drawing.Size(85, 64);
            this.ButtonCriarFunc.TabIndex = 6;
            this.ButtonCriarFunc.Text = "Funcionário";
            this.ButtonCriarFunc.UseVisualStyleBackColor = true;
            this.ButtonCriarFunc.Click += new System.EventHandler(this.ButtonCriarFunc_Click);
            // 
            // ButtonCriarDep
            // 
            this.ButtonCriarDep.Location = new System.Drawing.Point(29, 255);
            this.ButtonCriarDep.Name = "ButtonCriarDep";
            this.ButtonCriarDep.Size = new System.Drawing.Size(85, 63);
            this.ButtonCriarDep.TabIndex = 7;
            this.ButtonCriarDep.Text = "Departamento";
            this.ButtonCriarDep.UseVisualStyleBackColor = true;
            this.ButtonCriarDep.Click += new System.EventHandler(this.ButtonCriarDep_Click);
            // 
            // Criar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 410);
            this.Controls.Add(this.ButtonCriarDep);
            this.Controls.Add(this.ButtonCriarFunc);
            this.Controls.Add(this.GroupBoxCriarDepartamentos);
            this.Controls.Add(this.GroupBoxCriar);
            this.Name = "Criar";
            this.Text = "Criar";
            this.Load += new System.EventHandler(this.Criar_Load);
            this.GroupBoxCriarDepartamentos.ResumeLayout(false);
            this.GroupBoxCriarDepartamentos.PerformLayout();
            this.GroupBoxCriar.ResumeLayout(false);
            this.GroupBoxCriar.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GroupBoxCriarDepartamentos;
        private System.Windows.Forms.Button buttonCancelarCriarDepartamento;
        private System.Windows.Forms.Button ButtonCriarDepartamento;
        private System.Windows.Forms.TextBox TextBoxCriarSigla;
        private System.Windows.Forms.TextBox TextBoxCriarDepartamento;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox GroupBoxCriar;
        private System.Windows.Forms.Button ButtonCalcelarCriarFunc;
        private System.Windows.Forms.Button ButtonCriar;
        private System.Windows.Forms.ComboBox ComboBoxCriarDepartamentos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TextBoxCriarNome;
        private System.Windows.Forms.Button ButtonCriarFunc;
        private System.Windows.Forms.Button ButtonCriarDep;
    }
}