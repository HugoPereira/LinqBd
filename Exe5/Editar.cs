﻿using LinqBd;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exe5
{
    public partial class Editar : Form
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();

        public Editar()
        {
            InitializeComponent();
            dataGridView1.Visible = true;
            
        }

        private void Editar_Load(object sender, EventArgs e)
        {

            #region datagridview1
            //dataGridView1.Columns.Clear();
           

            dataGridView1.Columns.Add("colId", "ID"); //primeiro o nome da coluna e dps o titulo do cabeçalho da coluna

            dataGridView1.Columns.Add("colNome", "Nome");

            dataGridView1.Columns.Add("colDepartamento", "Departamento");

            var outraLista3 = from Funcionario in dc.Funcionarios select Funcionario;

            int idxlinha = 0;

            dataGridView1.Rows.Clear();

            foreach (Funcionario func in outraLista3)//por cada funcionario na lista
            {

                DataGridViewRow linha = new DataGridViewRow();//cria uma coluna

                //dataGridView1.Rows.Clear();

                dataGridView1.Rows.Add(linha);//insere as colunas na grid

                dataGridView1.Rows[idxlinha].Cells[0].Value = func.ID;// o valor da coluna 0, sera o id
                dataGridView1.Rows[idxlinha].Cells[1].Value = func.Nome;
                dataGridView1.Rows[idxlinha].Cells[2].Value = func.Departamento; //o valor da coluna 2, sera o departamento
                idxlinha++;
            }

            //ajusta o tamanho das colunas por codigo
            //dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            #endregion

            
        }

        private void funcionáriosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dataGridView1.Visible = true;
           
            GroupBoxFunc.Visible = false;
           

            #region datagridview1
            dataGridView1.Columns.Clear();
            dataGridView1.Rows.Clear();

            dataGridView1.Columns.Add("colId", "ID"); //primeiro o nome da coluna e dps o titulo do cabeçalho da coluna

            dataGridView1.Columns.Add("colNome", "Nome");

            dataGridView1.Columns.Add("colDepartamento", "Departamento");

            var outraLista3 = from Funcionario in dc.Funcionarios select Funcionario;

            int idxlinha = 0;

            //dataGridView1.Rows.Clear();

            foreach (Funcionario func in outraLista3)//por cada funcionario na lista
            {

                DataGridViewRow linha = new DataGridViewRow();//cria uma coluna

                //dataGridView1.Rows.Clear();

                dataGridView1.Rows.Add(linha);//insere as colunas na grid

                dataGridView1.Rows[idxlinha].Cells[0].Value = func.ID;// o valor da coluna 0, sera o id
                dataGridView1.Rows[idxlinha].Cells[1].Value = func.Nome;
                dataGridView1.Rows[idxlinha].Cells[2].Value = func.Departamento; //o valor da coluna 2, sera o departamento
                idxlinha++;
            }

            //ajusta o tamanho das colunas por codigo
            //dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            #endregion

        }

        private void departamentosToolStripMenuItem_Click(object sender, EventArgs e)
        {
           

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            GroupBoxFunc.Visible = true;
            TextBoxNome.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            ComboBoxDepartamento.DataSource = dc.Departamentos;
            ComboBoxDepartamento.DisplayMember = "Sigla";
            
            linha = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value);
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private int linha;

        private void ButtonGravar_Click(object sender, EventArgs e)
        {
            Funcionario funcionarioAAlterar = new Funcionario();

            var pesquisa = from Funcionario in dc.Funcionarios
                           where Funcionario.ID == linha
                           select Funcionario;
            


            funcionarioAAlterar = pesquisa.Single();//retorna o elemento da sequiencia, vai buscar o primeiro que encontrar
            if (string.IsNullOrEmpty(TextBoxNome.Text))
            {
                MessageBox.Show("Insira um nome");
                return;
            }
            if (string.IsNullOrEmpty(ComboBoxDepartamento.Text))
            {
                MessageBox.Show("escolha um departamento");
                return;
            }

           
            funcionarioAAlterar.Departamento = ComboBoxDepartamento.Text;//muda o departamento do elemento que encontrou
            funcionarioAAlterar.Nome = TextBoxNome.Text;

            try
            {
                dc.SubmitChanges();
            }
            catch (Exception p)
            {
                Console.WriteLine(p.Message);
            }

            Close();

        }
        
        private void ButtonGravarDep_Click(object sender, EventArgs e)
        {

        }

        private void CuttonClose_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Não foram efectuadas alterações.");
            
            GroupBoxFunc.Visible = false;
        }
    }

}
