﻿namespace Exe5
{
    partial class CRUDBd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonCriar = new System.Windows.Forms.Button();
            this.ButtonDelete = new System.Windows.Forms.Button();
            this.ButtonVer = new System.Windows.Forms.Button();
            this.ButtonUpdate = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ButtonCriar
            // 
            this.ButtonCriar.Image = global::Exe5.Properties.Resources.icon_create;
            this.ButtonCriar.Location = new System.Drawing.Point(49, 60);
            this.ButtonCriar.Name = "ButtonCriar";
            this.ButtonCriar.Size = new System.Drawing.Size(75, 56);
            this.ButtonCriar.TabIndex = 0;
            this.ButtonCriar.UseVisualStyleBackColor = true;
            this.ButtonCriar.Click += new System.EventHandler(this.ButtonCriar_Click_1);
            // 
            // ButtonDelete
            // 
            this.ButtonDelete.Image = global::Exe5.Properties.Resources.icon_delete;
            this.ButtonDelete.Location = new System.Drawing.Point(171, 172);
            this.ButtonDelete.Name = "ButtonDelete";
            this.ButtonDelete.Size = new System.Drawing.Size(75, 56);
            this.ButtonDelete.TabIndex = 1;
            this.ButtonDelete.UseVisualStyleBackColor = true;
            this.ButtonDelete.Click += new System.EventHandler(this.ButtonDelete_Click);
            // 
            // ButtonVer
            // 
            this.ButtonVer.Image = global::Exe5.Properties.Resources.icon_read;
            this.ButtonVer.Location = new System.Drawing.Point(171, 60);
            this.ButtonVer.Name = "ButtonVer";
            this.ButtonVer.Size = new System.Drawing.Size(75, 56);
            this.ButtonVer.TabIndex = 2;
            this.ButtonVer.UseVisualStyleBackColor = true;
            this.ButtonVer.Click += new System.EventHandler(this.ButtonVer_Click);
            // 
            // ButtonUpdate
            // 
            this.ButtonUpdate.Image = global::Exe5.Properties.Resources.icon_update;
            this.ButtonUpdate.Location = new System.Drawing.Point(49, 172);
            this.ButtonUpdate.Name = "ButtonUpdate";
            this.ButtonUpdate.Size = new System.Drawing.Size(75, 56);
            this.ButtonUpdate.TabIndex = 3;
            this.ButtonUpdate.UseVisualStyleBackColor = true;
            this.ButtonUpdate.Click += new System.EventHandler(this.ButtonUpdate_Click);
            // 
            // CRUDBd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(309, 278);
            this.Controls.Add(this.ButtonUpdate);
            this.Controls.Add(this.ButtonVer);
            this.Controls.Add(this.ButtonDelete);
            this.Controls.Add(this.ButtonCriar);
            this.Name = "CRUDBd";
            this.Text = "Crud";
            this.Load += new System.EventHandler(this.CRUDBd_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ButtonCriar;
        private System.Windows.Forms.Button ButtonDelete;
        private System.Windows.Forms.Button ButtonVer;
        private System.Windows.Forms.Button ButtonUpdate;
    }
}

