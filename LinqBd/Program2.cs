﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqBd
{
    class Program2
    {
        static void Main3(string[] args)
        {
            DataClasses1DataContext dc = new DataClasses1DataContext();//crio um objecto deste tipo

            //lista de funcionarios que trabalham em DF
            //var lista = from Funcionario in dc.Funcionarios where Funcionario.Departamento == "DF" select Funcionario;

            //foreach (Funcionario func in lista)//mostra todos os objecto funcionarios na lista
            //{
            //    Console.WriteLine("ID: " + func.ID);

            //    Console.WriteLine("Nome: " + func.Nome);

            //    Console.WriteLine("Departamento: " + func.Departamento);

            //    Console.WriteLine();
            //}

            //---------------------------------------------------

            //funcionarios com o nome comecado por jo
            //var lista = from Funcionario in dc.Funcionarios where Funcionario.Nome.StartsWith("Jo") select Funcionario;

            //foreach (Funcionario func in lista)//mostra todos os objecto funcionarios na lista
            //{
            //    Console.WriteLine("ID: " + func.ID);

            //    Console.WriteLine("Nome: " + func.Nome);

            //    Console.WriteLine("Departamento: " + func.Departamento);

            //    Console.WriteLine();
            //}
            //--------------------------------------------------

            //funcionarios com nome "ana"
            var lista = from Funcionario in dc.Funcionarios where Funcionario.Nome.Contains("ana") select Funcionario;

            foreach (Funcionario func in lista)//mostra todos os objecto funcionarios na lista
            {
                Console.WriteLine("ID: " + func.ID);

                Console.WriteLine("Nome: " + func.Nome);

                Console.WriteLine("Departamento: " + func.Departamento);

                Console.WriteLine();
            }

            Console.WriteLine("-----------------------------------------------");

            //agrupar informacao - contar funcionarios por departamento

            //crio uma nova lista, onde agrupo os funcionarios, do departamento num objecto chamado c
            var novaLista = from Funcionario in dc.Funcionarios
                            group Funcionario by Funcionario.Departamento
                            into c
                            select new 
                            {
                                Departamento = c.Key,
                                Contagem = c.Count()
                            };
            //cria um novo objecto chamado c e vou buscar o departamento, que vai ser igual ao novo objecto. key(sendo chave primaria)
            //e crio a contagem que vai ser igual ao c. contagem


            //var novaLista = from Funcionario in dc.Funcionarios
            //                group Funcionario by Funcionario.Departamento
            //                into c where c.Count() > 3 //posso filtrar ainda mais usando o where...
            //                select new
            //                {
            //                    Departamento = c.Key,
            //                    Contagem = c.Count()
            //                };

            foreach (var c in novaLista)//mostra todos os objecto funcionarios na lista
            {
                Console.WriteLine(c.Departamento +  " (" + c.Contagem + ")");
            }

            Console.WriteLine("----------------------Junção de 2 tabelas-------------------------");
            //join(junta) a tabela funcionarios e a tabela departamentos onde o departamento do funcionario seja igual a sigla do departamento(cruza sigla de um com departamento do outro)
            var outraLista = from Funcionario in dc.Funcionarios
                             join Departamento in dc.Departamentos
                             on Funcionario.Departamento equals Departamento.Sigla
                             select new
                             {
                                 Funcionario.ID,
                                 Funcionario.Nome,
                                 Departamento.Departamento1
                             };
            //cria um novo onde com 2 campos de uma tabelka e outro campo da outra tabela, neste caso ao campo departamento do departamento


            foreach (var c in outraLista)//mostra todos os objecto funcionarios na lista
            {
                Console.WriteLine("ID: " + c.ID);

                Console.WriteLine("Nome: " + c.Nome);

                Console.WriteLine("Departamento: " + c.Departamento1);

                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}
