﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqBd
{
    class ManipulaDados
    {
        static void Main(string[] args)
        {
            //manipulacao de dados
            DataClasses1DataContext dc = new DataClasses1DataContext();

            //inserir novo
            Funcionario func = new Funcionario//isto e uma classe
            {
                ID = dc.Funcionarios.Count() + 20,
                Nome = "Carlos Xavier",
                Departamento = "DF"
            };

            dc.Funcionarios.InsertOnSubmit(func);//insere o que criamos no diagrama que ciramo no inicio, fica pendente para inserir

            try
            {
                dc.SubmitChanges();//isto é que insere na base de dados o que esta pendente
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            var lista = from Funcionario in dc.Funcionarios select Funcionario;

            foreach (Funcionario funcionario in lista)//mostra todos os objecto funcionarios na lista
            {
                Console.WriteLine("ID: " + funcionario.ID);

                Console.WriteLine("Nome: " + funcionario.Nome);

                Console.WriteLine("Departamento: " + funcionario.Departamento);

                Console.WriteLine();
            }

            Console.WriteLine("Existem de momento {0} funcionários.", lista.Count());

            Console.WriteLine("-------------------------alterar--------------------------------");

            //alterar
            Funcionario funcionarioAAlterar = new Funcionario();

            var pesquisa = from Funcionario in dc.Funcionarios
                           where Funcionario.ID == 4
                           select Funcionario;

            funcionarioAAlterar = pesquisa.Single();//retorna o elemento da sequiencia, vai buscar o primeiro que encontrar

            funcionarioAAlterar.Departamento = "RH";//muda o departamento do elemento que encontrou

            try
            {
                dc.SubmitChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }


            var listaAlterada = from Funcionario in dc.Funcionarios select Funcionario;

            foreach (Funcionario funcionario in listaAlterada)//mostra todos os objecto funcionarios na lista
            {
                Console.WriteLine("ID: " + funcionario.ID);

                Console.WriteLine("Nome: " + funcionario.Nome);

                Console.WriteLine("Departamento: " + funcionario.Departamento);

                Console.WriteLine();
            }

            Console.WriteLine("-------------------------eliminar--------------------------------");

            //eliminar
           Funcionario f = new Funcionario();

            //depois de apagar vai dar exception
            var outraPesquisa = from Funcionario in dc.Funcionarios
                                where Funcionario.ID == 9
                                select Funcionario;
            
            f = outraPesquisa.Single();

            dc.Funcionarios.DeleteOnSubmit(f);//deixa o eliminar pendente

            try
            {
                dc.SubmitChanges();//actualiza a lista, ja como elemento apagado
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            var listaApagada  = from Funcionario in dc.Funcionarios select Funcionario;

            foreach (Funcionario funcionario in listaApagada)//mostra todos os objecto funcionarios na lista
            {
                Console.WriteLine("ID: " + funcionario.ID);

                Console.WriteLine("Nome: " + funcionario.Nome);

                Console.WriteLine("Departamento: " + funcionario.Departamento);

                Console.WriteLine();
            }

    
            Console.ReadKey();
        }
    }
}
