﻿using LinqBd;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LinqUi
{
    public partial class Form1 : Form
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();

        public Form1()
        {

            InitializeComponent();
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //listview
            //adicionamos as colunas
            listView1.Columns.Add("Id");
            listView1.Columns.Add("Nome");
            listView1.Columns.Add("Departamento");

            

            var lista = from Funcionario 
                        in dc.Funcionarios
                        select Funcionario;

            foreach (Funcionario func in lista)
            {
                ListViewItem item;//criamos um item do tipo listviewitem

                item = listView1.Items.Add(func.ID.ToString());//insermos os campos nas colunas, primeiro campo
                item.SubItems.Add(func.Nome);//campos restantes
                item.SubItems.Add(func.Departamento);
            }

           

            for (int idx = 0; idx <= 2; idx++)//ajustar as coluna ao conteudo
            {
                listView1.Columns[idx].AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
            }

            //---------------------------------------------
            //treeview

            //crio uma lista
            var outraLista = from Departamento 
                             in dc.Departamentos
                             select Departamento;

            //por cada objecto dentro da outralista
            foreach (Departamento dep in outraLista)
            {
                treeView1.Nodes.Add(dep.Sigla);//cria um no da arvore com as siglas
            }

            //segundo nivel da arvore = funcionarios

            //cria outra lista e ordena para funcao
            var outraLista2 = from Funcionario
                              in dc.Funcionarios
                              orderby Funcionario.Nome
                              select Funcionario;

            string departamento;//cria uma string departamento

            foreach (Funcionario func in outraLista2)//por cada funcionarios na outra lista
            {
                departamento = func.Departamento;//a string deparatamento e igual ao objecto

                foreach (TreeNode principal in treeView1.Nodes)//em cada nó da arvore na arvore
                {
                    if (principal.Text == departamento)//se o nó for igual a string dos departamentos
                    {
                        principal.Nodes.Add(func.ID +" - "+func.Nome);//metemos os nomes filtrado por cada nó
                    }
                }
            }

            //-------------------------------------------

            //gridview

            dataGridView1.Columns.Add("colId", "Id"); //primeiro o nome da coluna e dps o titulo do cabeçalho da coluna
            dataGridView1.Columns.Add("colNome", "Nome");
            dataGridView1.Columns.Add("colDepartamento", "Departamento");

            var outraLista3 = from Funcionario in dc.Funcionarios select Funcionario;//carrega a lista

            int idxlinha = 0;

            DataGridViewCellStyle vermelho = new DataGridViewCellStyle();//cria um estilo de linha

            vermelho.ForeColor = Color.Red;//igual a vermelho

            foreach (Funcionario func in outraLista3)//por cada funcionario na lista
            {
                DataGridViewRow linha = new DataGridViewRow();//cria uma coluna

                dataGridView1.Rows.Add(linha);//insere as colunas na grid

                dataGridView1.Rows[idxlinha].Cells[0].Value = func.ID;// o valor da coluna 0, sera o id
                dataGridView1.Rows[idxlinha].Cells[1].Value = func.Nome;//o valor da coluna 1, sera o nome
                dataGridView1.Rows[idxlinha].Cells[2].Value = func.Departamento; //o valor da coluna 2, sera o departamento

                if ((string) dataGridView1.Rows[idxlinha].Cells[2].Value == "DC")//fazemos o cast para forcar ele a ver o value da grid como sendo uma string
                {
                    //se o departamneto for = DC
                    dataGridView1.Rows[idxlinha].DefaultCellStyle = vermelho;//mete as linhas correspondentes a vermelho

                    //cor da linha a amarelo
                    dataGridView1.Rows[idxlinha].DefaultCellStyle.BackColor = Color.Yellow;
                }

                idxlinha++;//incrementa a linha para colocar todos os funcionarios
            }

            //ajusta o tamanho das colunas por codigo
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;              
           

        }
        private bool statusID = true;
        private bool statusNome = true;
        private bool statusDepartamento = true;
        private void listView1_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            
            
            switch (e.Column)
            {
                case 0:
                    if (!statusID)
                    {
                        
                        listView1.Items.Clear();
                        var lista = from Funcionario
                            in dc.Funcionarios
                                    orderby Funcionario.ID ascending
                                    select Funcionario;
                        foreach (Funcionario func in lista)
                        {
                            ListViewItem item;//criamos um item do tipo listviewitem

                            item = listView1.Items.Add(func.ID.ToString());//insermos os campos nas colunas, primeiro campo
                            item.SubItems.Add(func.Nome);//campos restantes
                            item.SubItems.Add(func.Departamento);
                        }
                        statusID = true;
                        return;
                    }
                    if(statusID)
                    {
                        
                        listView1.Items.Clear();
                        var lista = from Funcionario
                            in dc.Funcionarios
                                    orderby Funcionario.ID descending
                                    select Funcionario;
                        foreach (Funcionario func in lista)
                        {
                            ListViewItem item;//criamos um item do tipo listviewitem

                            item = listView1.Items.Add(func.ID.ToString());//insermos os campos nas colunas, primeiro campo
                            item.SubItems.Add(func.Nome);//campos restantes
                            item.SubItems.Add(func.Departamento);
                        }
                        statusID = false;
                        return;
                    }
                    
                    break;

                case 1:
                    if (!statusNome)
                    {
                        listView1.Items.Clear();
                        var lista = from Funcionario
                            in dc.Funcionarios
                                    orderby Funcionario.Nome ascending
                                    select Funcionario;

                        foreach (Funcionario func in lista)
                        {
                            ListViewItem item;//criamos um item do tipo listviewitem


                            item = listView1.Items.Add(func.ID.ToString());//insermos os campos nas colunas, primeiro campo
                            item.SubItems.Add(func.Nome);//campos restantes
                            item.SubItems.Add(func.Departamento);
                        }
                        statusNome = true;
                        return;
                    }
                    if (statusNome)
                    {
                        listView1.Items.Clear();
                        var lista = from Funcionario
                            in dc.Funcionarios
                                    orderby Funcionario.Nome descending
                                    select Funcionario;

                        foreach (Funcionario func in lista)
                        {
                            ListViewItem item;//criamos um item do tipo listviewitem


                            item = listView1.Items.Add(func.ID.ToString());//insermos os campos nas colunas, primeiro campo
                            item.SubItems.Add(func.Nome);//campos restantes
                            item.SubItems.Add(func.Departamento);
                        }
                        statusNome = false;
                        return;
                    }
                        break;
                  

                case 2:
                    if (!statusDepartamento)
                    {
                        listView1.Items.Clear();
                        var lista = from Funcionario
                            in dc.Funcionarios
                                    orderby Funcionario.Departamento ascending
                                    select Funcionario;
                        foreach (Funcionario func in lista)
                        {
                            ListViewItem item;//criamos um item do tipo listviewitem

                            item = listView1.Items.Add(func.ID.ToString());//insermos os campos nas colunas, primeiro campo
                            item.SubItems.Add(func.Nome);//campos restantes
                            item.SubItems.Add(func.Departamento);
                        }
                        statusDepartamento = true;
                        return;
                    }

                    if (statusDepartamento)
                    {
                        listView1.Items.Clear();
                        var lista = from Funcionario
                            in dc.Funcionarios
                                    orderby Funcionario.Departamento descending
                                    select Funcionario;
                        foreach (Funcionario func in lista)
                        {
                            ListViewItem item;//criamos um item do tipo listviewitem

                            item = listView1.Items.Add(func.ID.ToString());//insermos os campos nas colunas, primeiro campo
                            item.SubItems.Add(func.Nome);//campos restantes
                            item.SubItems.Add(func.Departamento);
                        }

                        statusDepartamento = false;
                        return;
                    }
                    break;

            }

        }
    }
}
